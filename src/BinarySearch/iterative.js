/**
 * Perform Binary search by iterative approach.
 * To perform binary search the input array has to be sorted.
 * low = startIndex, high = endIndex, mid = Math.floor((low+hight)/2);
 */
const a = [3, 6, 8, 12, 14, 17, 25, 29, 31, 36, 42, 47, 53, 55, 62];

function binarySearch(a, key) {
    let low = 0;
    let high = a.length - 1;

    while(low <= high) {
        let mid = Math.floor((low + high) / 2);
        console.log(low, high, mid, a);

        if (key == a[mid]) {
            return mid;
        } else if (key < a[mid]) {
            high = mid - 1;
        } else {
            low = mid + 1;
        }
    }
    return -1;
}

console.log('Result is', binarySearch(a, 18));